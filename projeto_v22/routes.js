var express = require('express');
var router = express.Router();
const path = require('path');

Usuario =require('./models/usuario');
Evento =require('./models/evento');
Recibo =require('./models/recibo');
EventoSalvo =require('./models/eventoSalvo');
Vaga =require('./models/vaga');
VagaSalva =require('./models/vagaSalva');
Email =require('./models/email');
Contato =require('./models/contato');

function isUserAuthenticated(req, res, callback){
  console.log(req);
  if(req.isAuthenticated()) {
      console.log("user: "+req.isAuthenticated());
      callback();
  } else {
      res.redirect('/');
  }
}

//USUÁRIO
module.exports=function(passport){
   /* GET home page. */
  router.get('/', function(req, res, next) {    
    res.sendFile(path.join(__dirname,'./client','home.html'));
    });
     
    router.get('/api/usuarios', (req, res) => {      
        Usuario.getUsuarios(req, res);
    });
// router.post('/usuario/login',passport.authenticate('loginUsuario',{
//     successRedirect:'/#/usuarios/details',
//     failureRedirect:'/#/usuarios/details'})
// );
// router.post('/usuario/login',passport.authenticate('loginUsuario',{
//     successRedirect:'/#/usuarios/details',
//     failureRedirect:'/#/usuarios/details'})
// );
router.post('/usuario/login', passport.authenticate('loginUsuario'),(req,res)=>
{
  if(req.isAuthenticated()) {
    console.log("autenticou");
    res.status(200).json(req.user); 
  } 
  else{
    console.log(" nao autenticou");
    res.status(500).json(req.user); 
  }
});

router.get('/usuario/logout',isUserAuthenticated, function(req, res) {
  console.log("Logout");
  req.logout();
  res.status(200).json("Sessão encerrada");
  //redirect('/#!');
});
// router.post('/usuario/login',passport.authenticate('loginUsuario'),(req,res)=>
// {
//  //if(req.isAuthenticated()) {
//     console.log("autenticou");
//     res.status(200).json(req.user); 
//   } 
//   else{
//     console.log(" nao autenticou");
//     res.status(500).json(req.user); 
//   }
// });

router.get('/detalhes/usuario/:id',isUserAuthenticated, (req, res) => {
  Usuario.getUsuarioById(req, res);
});

router.post('/add/usuario', (req, res) => {	
	Usuario.addUsuario(req, res);
});

router.put('/editar/usuario/:id',isUserAuthenticated, (req, res) => {
  Usuario.updateUsuario(req, res);
});

router.delete('/deletar/usuario/:id', isUserAuthenticated, (req, res) => {
  Usuario.removeUsuario(req, res);	
});

//EVENTO
router.get('/listar/eventos',isUserAuthenticated, (req, res) => {  
	Evento.getEventos(req, res);  
});

router.get('/listar/eventos/cadastrados/:id',isUserAuthenticated, (req, res) => { 
	Evento.getEventosCadastrados(req, res);  
});

router.get('/detalhes/evento/:id',isUserAuthenticated, (req, res) => {   
  Evento.getEventoById(req, res);  
});

router.get('/detalhes/evento/cadastrado/:id',isUserAuthenticated, (req, res) => {   
  Evento.getEventoById(req, res);  
});

router.post('/add/evento/:id', isUserAuthenticated, (req, res) => {	
	Evento.addEvento(req, res);
});

router.put('/editar/evento/:id',isUserAuthenticated, (req, res) => {  
  Evento.updateEvento(req, res);
});

router.delete('/deletar/evento/:id', isUserAuthenticated, (req, res) => {
  Evento.removeEvento(req, res);	
});

router.post('/pesquisar/eventos/',isUserAuthenticated, (req, res) => {  
  Evento.searchEventos(req, res);	
});

// router.post('/pesquisar/eventos/adicionados/',isUserAuthenticated, (req, res) => {  
//   Evento.searchEventosAdicionados(req, res);	
// });

//RECIBO
router.post('/add/recibo/:id/:idEvento', isUserAuthenticated, (req, res) => { 
	Recibo.addRecibo(req, res);
});

router.get('/listar/eventos/adicionados/:id',isUserAuthenticated, (req, res) => {  
  Recibo.getEventosAdicionados(req, res);  
});

router.get('/listar/recibos/:idUser/:idEvento',isUserAuthenticated, (req, res) => {  
  Recibo.getRecibos(req, res);  
});

router.put('/editar/recibo/:id',isUserAuthenticated, (req, res) => {    
  Recibo.updateRecibo(req, res);  
});

// router.delete('/deletar/recibo/:id',isUserAuthenticated, (req, res) => {    
//   Recibo.removeRecibo(req, res);  
// });

//EventoSalvo
router.post('/add/evento/salvo/:idEvento/:idUser',isUserAuthenticated, (req, res) => {  
  EventoSalvo.addEvento(req, res);	
});

router.get('/listar/eventos/salvos/:id',isUserAuthenticated, (req, res) => {    
  EventoSalvo.getEventosSalvos(req, res);	
});

router.delete('/deletar/evento/salvo/:id/:idEvento',isUserAuthenticated, (req, res) => {  
  EventoSalvo.removeEventoSalvo(req, res);	
});

router.get('/detalhes/evento/salvo/:id/:idEvento',isUserAuthenticated, (req, res) => {  
  EventoSalvo.getEventoSalvoById(req, res);	
});

//Vaga
router.post('/add/vaga/:id', (req, res) => {	
	Vaga.addVaga(req, res);
});

router.get('/listar/vagas/:idEvento',isUserAuthenticated, (req, res) => {
  Vaga.getVagas(req, res);
});

router.get('/detalhes/vaga/:id',isUserAuthenticated, (req, res) => {
  Vaga.getVagaById(req, res);
});

router.put('/editar/vaga/:id',isUserAuthenticated, (req, res) => {
  Vaga.updateVaga(req, res);
});

router.delete('/deletar/vaga/:id', isUserAuthenticated, (req, res) => {
  Vaga.removeVaga(req, res);	
});

//VagaSalva
router.post('/add/vaga/salva/:idEvento/:idUser',isUserAuthenticated, (req, res) => {  
  VagaSalva.addVaga(req, res);	
});

router.get('/listar/vagas/salvas/:id',isUserAuthenticated, (req, res) => {    
  VagaSalva.getVagasSalvas(req, res);	
});

router.get('/listar/vagas/salvas/:idUser/:idEvento',isUserAuthenticated, (req, res) => {    
  VagaSalva.getVagasSalvasById(req, res);	
});

router.delete('/deletar/vaga/salva/:id',isUserAuthenticated, (req, res) => {
  VagaSalva.removeVagasSalvas(req, res);	
});

//E-mail
router.post('/api/email/:emailRemetente',isUserAuthenticated, (req, res) => { 
  Email.enviar(req,res); 
});

//Contato
router.post('/api/contato/:emailRemetente',isUserAuthenticated, (req, res) => { 
  Contato.enviar(req,res); 
});

return router;
}