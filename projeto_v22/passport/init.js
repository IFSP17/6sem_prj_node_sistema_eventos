var login = require('./login');

module.exports = function(passport){
  passport.serializeUser(function(user, callback){
    callback(null, user.id);
  });
  passport.deserializeUser(function(id, callback){
    callback(null, {id : id, name:id});
  });

  // Setting up Passport Strategies for Login and SignUp/Registration
  login(passport);
  //signup(passport)

}