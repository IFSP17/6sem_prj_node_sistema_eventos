const pg = require('pg');
const connectionString = process.env.DATABASE_URL || 'postgres://postgres:123456@localhost:5432/Evento';

const client = new pg.Client(connectionString);
client.connect();
const query = client.query(
  'CREATE TABLE IF NOT EXISTS "Usuario" (id SERIAL PRIMARY KEY, nome character varying(50) NOT NULL, "telefoneFixo" character varying(45) NULL,'+
  '"telefoneMovel" character varying(45) NOT NULL, setor character varying(45) NULL, email character varying(100) NOT NULL, senha character varying(45) NOT NULL, cpf_cnpj character varying(25) NOT NULL) WITH ( OIDS=FALSE);');
  query.on('end', () => { client.end(); });

// GET USUÁRIO
module.exports.getUsuarios = (req, res) => {	
  var results;

  // Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {
    var email = localStorage.getItem('email');      
    console.log("email"+email);    
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }    
    // SQL Query > Select Data
    var query = client.query('SELECT * FROM "Usuario" WHERE email=($1)', [email]);
    // Stream results back one row at a time
    query.on('row', (row) => {
      results=row;      		
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
      done();	      		
      return res.json(results);
    });
  });
}


module.exports.getUsuarioById = (req, res) => {
	var result;
  // Grab data from the URL parameters
  const id = req.params.id;
  // Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    // SQL Query > Delete Data    
    // SQL Query > Select Data
    var query = client.query('SELECT * FROM "Usuario" WHERE id=($1)', [id]);
    // Stream results back one row at a time
    query.on('row', (row) => {
      result=row;	     	
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
			done();			
      return res.json(result);
    });
  });	
}	

// ADD USUÁRIO 
module.exports.addUsuario = (req, res) => {  
	const results = [];
  // Grab data from http request
  const data = {nome: req.body.nome, telefoneFixo: req.body.telefoneFixo, telefoneMovel: req.body.telefoneMovel,
                email: req.body.email, senha: req.body.senha, setor: req.body.setor, cpf_cnpj:req.body.cpf_cnpj};
  // Get a Postgres client from the connection pool
  if(data.setor=="Nenhum" || data.setor==undefined){
    data.setor="Nenhum";
  }
  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    // SQL Query > Insert Data
  client.query('INSERT INTO "Usuario" (nome,"telefoneFixo","telefoneMovel",'+
    'email,senha,setor, "cpf_cnpj") values($1, $2, $3, $4, $5, $6, $7)',
    [data.nome,data.telefoneFixo,data.telefoneMovel, data.email, data.senha, 
     data.setor, data.cpf_cnpj]);
    // SQL Query > Select Data
    const query = client.query('SELECT * FROM "Usuario" ORDER BY id ASC');
    // Stream results back one row at a time
    query.on('row', (row) => {
      results.push(row);
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
}

  // UPDATE USUÁRIO
  module.exports.updateUsuario = (req,res) => {
    const results=[];	
    const id = req.params.id;
    // Grab data from http request
    const data = {nome: req.body.nome, telefoneFixo: req.body.telefoneFixo, telefoneMovel: req.body.telefoneMovel, email: req.body.email, senha: req.body.senha, setor: req.body.setor, cpf_cnpj:req.body.cpf_cnpj};
    // Get a Postgres client from the connection pool
    pg.connect(connectionString, (err, client, done) => {
      // Handle connection errors
      if(err) {
        done();
        console.log(err);
        return res.status(500).json({success: false, data: err});
      }
      // SQL Query > Update Data
      client.query('UPDATE "Usuario" SET nome=($1), "telefoneFixo"=($2), "telefoneMovel"=($3), email=($4), senha=($5), setor=($6), cpf_cnpj=($7)  WHERE id=($8)',
      [data.nome,data.telefoneFixo,data.telefoneMovel, data.email, data.senha, 
      data.setor, data.cpf_cnpj, id]);
      // SQL Query > Select Data
      const query = client.query('SELECT * FROM "Usuario" where id=($1)',[id]);
      // Stream results back one row at a time
      query.on('row', (row) => {
        results.push(row);
      });
      // After all data is returned, close connection and return results
      query.on('end', function() {
      done();   
      return res.json(results);	  
      });
  });
}


  // DELETE USUÁRIO
module.exports.removeUsuario = (req, res) => {
  const results = [];
  // Grab data from the URL parameters
  const id = req.params.id;
  // Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {
  // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    // SQL Query > Delete Data
    client.query('DELETE FROM "Usuario" WHERE id=($1)', [id]);
    // SQL Query > Select Data
    var query = client.query('SELECT * FROM "Usuario" ORDER BY id ASC');
    // Stream results back one row at a time
    query.on('row', (row) => {
      results.push(row);
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
}

