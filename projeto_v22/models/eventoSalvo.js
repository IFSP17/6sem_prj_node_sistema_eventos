const pg = require('pg');
const connectionString = process.env.DATABASE_URL || 'postgres://postgres:123456@localhost:5432/Evento';

const client = new pg.Client(connectionString);
client.connect();

//Salva eventos numa lista
module.exports.addEvento = (req, res) => {    
    var results = [];
    const idUser = req.params.idUser;
    const idEvento = req.params.idEvento;   
    // Get a Postgres client from the connection pool
    pg.connect(connectionString, (err, client, done) => {
        // Handle connection errors
        if(err) {
          done();
          console.log(err);
          return res.status(500).json({success: false, data: err});
        }
        // SQL Query > Insert Data
      client.query('INSERT INTO "Evento_Salvo"("idEvento","idUsuario") values($1, $2)',
      [idEvento, idUser]);
        // SQL Query > Select Data
        const query = client.query('SELECT * FROM "Evento_Salvo" ORDER BY "idEvento" ASC');
        // Stream results back one row at a time
        query.on('row', (row) => {
          results.push(row);
        });
        // After all data is returned, close connection and return results
        query.on('end', () => {
          done();
          return res.json(results);
        });
    });
}    
        
//Seleciona eventos salvos
module.exports.getEventosSalvos = (req, res) => {  
    var results = [];
    const id = req.params.id;    
    // Get a Postgres client from the connection pool
    pg.connect(connectionString, (err, client, done) => {
      // Handle connection errors
        if(err) {
            done();
            console.log(err);
            return res.status(500).json({success: false, data: err});
        }      
 
       const query =  client.query('SELECT * FROM "Evento" INNER JOIN "Evento_Salvo" ON "Evento".id="Evento_Salvo"."idEvento" and "Evento_Salvo"."idUsuario"=($1)',[id]);
        query.on('row', (row) => {
            results.push(row);             
        });
        // After all data is returned, close connection and return results
        query.on('end', () => {
            done();    
            return res.json(results);
        });
    }); 
}

 // DELETE evento salvo
 module.exports.removeEventoSalvo = (req, res) => {
   const results = [];
    // Grab data from the URL parameters
    const id = req.params.id;
    const idEvento = req.params.idEvento;    
    // Get a Postgres client from the connection pool
    pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
      if(err) {
        done();
        console.log(err);
        return res.status(500).json({success: false, data: err});
      }
      // SQL Query > Delete Data
      client.query('DELETE FROM "Evento_Salvo" WHERE "idUsuario"=($1) and "idEvento"=($2)', [id, idEvento]);
      // SQL Query > Select Data
      var query = client.query('SELECT * FROM "Evento_Salvo" ORDER BY "idEvento" ASC');
      // Stream results back one row at a time
      query.on('row', (row) => {
        results.push(row);
      });
      // After all data is returned, close connection and return results
      query.on('end', () => {
        done();
        return res.json(results);
      });
    });    
  }
  
  //Get evento salvo by id 
 module.exports.getEventoSalvoById = (req, res) => {
  const results = [];
   // Grab data from the URL parameters
   const id = req.params.id;
   const idEvento = req.params.idEvento;   
   // Get a Postgres client from the connection pool
   pg.connect(connectionString, (err, client, done) => {
   // Handle connection errors
     if(err) {
       done();
       console.log(err);
       return res.status(500).json({success: false, data: err});
     }
     // SQL Query > Delete Data
     var query = client.query('SELECT FROM "Evento_Salvo" WHERE "idUsuario"=($1) and "idEvento"=($2)', [id, idEvento]);
      query.on('row', (row) => {
       results.push(row);
     });
     // After all data is returned, close connection and return results
     query.on('end', () => {
       done();
       return res.json(results);
     });
   });
  }