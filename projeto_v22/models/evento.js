const pg = require('pg');
const connectionString = process.env.DATABASE_URL || 'postgres://postgres:123456@localhost:5432/Evento';

// Cria tabela de eventos
const client = new pg.Client(connectionString);
client.connect();
// const query = client.query(
//   'CREATE TABLE IF NOT EXISTS "Evento" (id SERIAL PRIMARY KEY, nome character varying(45) NOT NULL, hora character varying(45) NOT NULL, rua character varying(100),'+
//   'numero character varying(45), bairro character varying(100), cidade character varying(45), categoria character varying(45),'+
//   '"valorHomem" double precision, "valorMulher" character varying(45), "valorIdoso" character varying(45), "valorCrianca" character varying(45),'+
//   '"dataLimitePag" date, "vendaOnline" boolean, descricao character varying(5000), telefone character varying(45), email character varying(45),'+
//    '"duracao" integer NOT NULL, parcela1 double precision, parcela2 double precision, parcela3 double precision, parcela4 double precision,"quantConvites" integer,'+
//    ' "locaisFisicos" character varying(100000), "quantParcela1" integer, "quantParcela2" integer, "quantParcela3" integer,'+
//    '"quantParcela4" integer, "capacidadeMax" integer, data date, "dataVendas" date, tipo character varying (30) not null) WITH ( OIDS=FALSE)');
//   query.on('end', () => { client.end(); });

// Seleciona todos os eventos
module.exports.getEventos = (req, res) => {	
  const results = [];
  pg.connect(connectionString, (err, client, done) => {   
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    } 
    const query = client.query('SELECT * FROM "Evento" ORDER BY id ASC;');
    query.on('row', (row) => {      
    results.push(row);		  
    });
    query.on('end', () => {
      done();
     return res.json(results);
    });
  });
}

module.exports.getEventosCadastrados = (req, res) => {	
  const results = [];
  const id = req.params.id;   
  pg.connect(connectionString, (err, client, done) => {     
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }   
  
    const query = client.query('SELECT * FROM "Evento" where "idUsuario" = ($1);', [id]);   
    query.on('row', (row) => {
      results.push(row);		  
    });   
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
}

module.exports.getEventoById = (req, res) => {
	var result;
  const id = req.params.id;
  pg.connect(connectionString, (err, client, done) => {   
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }   
    var query = client.query('SELECT * FROM "Evento" WHERE id=($1)', [id]);  
    query.on('row', (row) => {
      result=row;           		
    });
    query.on('end', () => {
			done();			
      return res.json(result);
    });
  });	
}	

module.exports.addEvento = (req, res) => {  
  const results = [];
  const id = req.params.id;
  const data = {nome:req.body.nome, hora:req.body.hora, rua:req.body.rua, numero:req.body.numero, bairro:req.body.bairro,
cidade:req.body.cidade, categoria:req.body.categoria, valorHomem:req.body.valorHomem, valorMulher:req.body.valorMulher, valorIdoso:req.body.valorIdoso,
valorCrianca:req.body.valorCrianca, dataLimitePag:req.body.dataLimitePag, vendaOnline:req.body.vendaOnline, descricao:req.body.descricao,
telefone:req.body.telefone, email:req.body.email, duracao:req.body.duracao, parcela1:req.body.parcela1, parcela2:req.body.parcela2, 
parcela3:req.body.parcela3, parcela4:req.body.parcela4, quantConvites:req.body.quantConvites, locaisFisicos:req.body.locaisFisicos,
quantParcela1:req.body.quantParcela1, quantParcela2:req.body.quantParcela2, quantParcela3:req.body.quantParcela3, quantParcela4:req.body.quantParcela4,
capacidadeMax:req.body.capacidadeMax, data:req.body.data, dataVendas:req.body.dataVendas, tipo:req.body.tipo, duracaoDia:req.body.duracaoDia, cep:req.body.cep,
valorEstudante:req.body.valorEstudante, gratuito:req.body.gratuito};

  if(data.tipo==null || data.tipo=="" || data.tipo==undefined || data.tipo =="Aberto"){
    data.tipo="aberto";
  }
  pg.connect(connectionString, (err, client, done) => {   
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }      
    client.query('INSERT INTO "Evento" (nome, hora, rua, numero, bairro, cidade, categoria,"valorHomem", "valorMulher",'+
    '"valorIdoso", "valorCrianca","dataLimitePag", "vendaOnline", descricao, telefone, email,"duracao",'+
    'parcela1, parcela2, parcela3, parcela4, "quantConvites", "locaisFisicos", "quantParcela1", "quantParcela2",'+
    '"quantParcela3","quantParcela4", "capacidadeMax", data, "dataVendas", tipo, "idUsuario", "duracaoDia", cep, "valorEstudante", gratuito)'+ 
    'values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24,'+
    '$25, $26, $27, $28, $29, $30, $31, $32, $33, $34, $35, $36)',
    [data.nome, data.hora, data.rua, data.numero, data.bairro,data.cidade, data.categoria, data.valorHomem, data.valorMulher, data.valorIdoso,
    data.valorCrianca, data.dataLimitePag, data.vendaOnline, data.descricao, data.telefone, data.email, data.duracao, data.parcela1, data.parcela2, 
    data.parcela3, data.parcela4, data.quantConvites, data.locaisFisicos,
    data.quantParcela1, data.quantParcela2, data.quantParcela3, data.quantParcela4,
    data.capacidadeMax, data.data, data.dataVendas, data.tipo, id, data.duracaoDia, data.cep, data.valorEstudante, data.gratuito]);  
    const query = client.query('SELECT * FROM "Evento" ORDER BY id ASC'); 
    query.on('row', (row) => {
      results.push(row);     
    });
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
}

module.exports.updateEvento = (req,res) => {
 	const results = [];		
  const id = req.params.id;  

  const data = {nome:req.body.nome, hora:req.body.hora, rua:req.body.rua, numero:req.body.numero, bairro:req.body.bairro,
  cidade:req.body.cidade, categoria:req.body.categoria, valorHomem:req.body.valorHomem, valorMulher:req.body.valorMulher, valorIdoso:req.body.valorIdoso,
  valorCrianca:req.body.valorCrianca, dataLimitePag:req.body.dataLimitePag, vendaOnline:req.body.vendaOnline, descricao:req.body.descricao,
  telefone:req.body.telefone, email:req.body.email, duracao:req.body.duracao, parcela1:req.body.parcela1, parcela2:req.body.parcela2, 
  parcela3:req.body.parcela3, parcela4:req.body.parcela4, quantConvites:req.body.quantConvites, locaisFisicos:req.body.locaisFisicos,
  quantParcela1:req.body.quantParcela1, quantParcela2:req.body.quantParcela2, quantParcela3:req.body.quantParcela3, quantParcela4:req.body.quantParcela4,
  capacidadeMax:req.body.capacidadeMax, data:req.body.data, dataVendas:req.body.dataVendas, tipo:req.body.tipo, duracaoDia:req.body.duracaoDia, cep:req.body.cep,
  valorEstudante:req.body.valorEstudante, gratuito:req.body.gratuito};

  console.log("data"+ JSON.stringify(data));
  pg.connect(connectionString, (err, client, done) => {
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }    
  
    client.query('UPDATE "Evento" SET nome=($1), hora=($2), rua=($3), numero=($4), bairro=($5), cidade=($6), categoria=($7),"valorHomem"=($8), "valorMulher"=($9),'+
    '"valorIdoso"=($10), "valorCrianca"=($11),"dataLimitePag"=($12), "vendaOnline"=($13), descricao=($14), telefone=($15), email=($16),"duracao"=($17),'+
     'parcela1=($18), parcela2=($19), parcela3=($20), parcela4=($21), "quantConvites"=($22), "locaisFisicos"=($23), "quantParcela1"=($24), "quantParcela2"=($25), "quantParcela3"=($26),'+
     '"quantParcela4"=($27), "capacidadeMax"=($28), data=($29), "dataVendas"=($30), tipo=($31), "duracaoDia"=($32), cep=($33), "valorEstudante"=($34),'+
     'gratuito=($35) WHERE id=($36)',
      [data.nome, data.hora, data.rua, data.numero, data.bairro, data.cidade, data.categoria,data.valorHomem,data.valorMulher, data.valorIdoso,
      data.valorCrianca, data.dataLimitePag, data.vendaOnline, data.descricao, data.telefone, data.email, data.duracao, data.parcela1, data.parcela2, 
      data.parcela3, data.parcela4, data.quantConvites, data.locaisFisicos,
      data.quantParcela1, data.quantParcela2, data.quantParcela3, data.quantParcela4,
      data.capacidadeMax, data.data, data.dataVendas, data.tipo, data.duracaoDia, data.cep, data.valorEstudante,data.gratuito, id]);

		const query = client.query('SELECT * FROM "Evento" ORDER BY id ASC');   
    query.on('row', (row) => {
      results.push(row);      
    });  
    query.on('end', function() {
    done();    
	  return res.json(results);	  
    });
  });
}

module.exports.removeEvento = (req, res) => {
	const results = [];
  const id = req.params.id; 
  pg.connect(connectionString, (err, client, done) => {  
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }   
    client.query('DELETE FROM "Evento" WHERE id=($1)', [id]);
    var query = client.query('SELECT * FROM "Evento" ORDER BY id ASC');  
    query.on('row', (row) => {
      results.push(row);
    });
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
}

module.exports.searchEventos = (req, res) => {
	const results = [];
  const data = {tipoPesquisa:req.body.tipoPesquisa, valorPesquisa:req.body.valorPesquisa};
  nome= data.valorPesquisa.split('');
  var inicio= nome[0]+nome[1]+nome[2]+nome[3];
  
  pg.connect(connectionString, (err, client, done) => {
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    } 

    if(data.tipoPesquisa=='Nome' || data.tipoPesquisa==undefined){      
      
      var query = client.query('SELECT * FROM "Evento" WHERE nome ilike $1', [inicio+'%']);   
    
      query.on('row', (row) => {
        results.push(row);     
      });   
      query.on('end', () => {
        done();      
        return res.json(results);
      });
    }

    if(data.tipoPesquisa=='Cidade'){
      var query = client.query('SELECT * FROM "Evento" WHERE cidade ilike $1', [inicio+'%']);
      query.on('row', (row) => {
      results.push(row);     
    });
      query.on('end', () => {
        done();      
        return res.json(results);
      });
    }

        if(data.tipoPesquisa=='Categoria'){      
      
          var query = client.query('SELECT * FROM "Evento" WHERE categoria ilike $1', [inicio+'%']);    
          query.on('row', (row) => {
            results.push(row);     
          });     
          query.on('end', () => {
            done();      
            return res.json(results);
          });
      }

      if(data.tipoPesquisa=='Ano'){    

        var query = client.query('SELECT * FROM "Evento" WHERE data ilike $1', ['%'+data.valorPesquisa]);   
        // Stream results back one row at a time
        query.on('row', (row) => {
          results.push(row);     
        });
        // After all data is returned, close connection and return results
        query.on('end', () => {
          done();      
          return res.json(results);
        });
      }

      if(data.tipoPesquisa=='Mes'){      
        
        // SQL Query > Select Data
        var query = client.query('SELECT * FROM "Evento" WHERE data ilike $1', ['%'+data.valorPesquisa+'%']);   
        // Stream results back one row at a time
        query.on('row', (row) => {
          results.push(row);     
        });
        // After all data is returned, close connection and return results
        query.on('end', () => {
          done();      
          return res.json(results);
        });
    }

    if(data.tipoPesquisa=='Dia'){      
      
      // SQL Query > Select Data
      var query = client.query('SELECT * FROM "Evento" WHERE data ilike $1', ['%'+data.valorPesquisa+'%']);   
      // Stream results back one row at a time
      query.on('row', (row) => {
        results.push(row);     
      });
      // After all data is returned, close connection and return results
      query.on('end', () => {
        done();      
        return res.json(results);
      });
    }
  });
}

//Pesquisa os eventos adicionados
module.exports.searchEventosAdicionados = (req, res) => {
	const results = [];
  // Grab data from the URL parameters
  const data = {tipoPesquisa:req.body.tipoPesquisa, valorPesquisa:req.body.valorPesquisa}  
  // Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }

    if(data.tipoPesquisa=='Nome'){         
      // SQL Query > Select Data
      var query = client.query('SELECT * FROM "Evento" WHERE nome=($1)', [data.valorPesquisa]);   
      // Stream results back one row at a time
      query.on('row', (row) => {
      results.push(row);     
      });
      // After all data is returned, close connection and return results
      query.on('end', () => {
        done();    
        return res.json(results);
      });
    }
  });
}

