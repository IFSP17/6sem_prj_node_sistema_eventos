const pg = require('pg');
const connectionString = process.env.DATABASE_URL || 'postgres://postgres:123456@localhost:5432/Evento';

const client = new pg.Client(connectionString);
client.connect();

// GET Vaga
module.exports.getVagas = (req, res) => {	
  var results= [];
  const idEvento = req.params.idEvento;
  // Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {   
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }    
    // SQL Query > Select Data
    var query = client.query('SELECT * FROM "Vagas" WHERE "idEvento"=($1)',[idEvento]);
    // Stream results back one row at a time
    query.on('row', (row) => {
      results.push(row);           		
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
      done();  	      		
      return res.json(results);
    });
  });
}

//Get vaga by id 
module.exports.getVagaById = (req, res) => {
	var result;
  // Grab data from the URL parameters
  const id = req.params.id;
  // Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    // SQL Query > Delete Data    
    // SQL Query > Select Data
    var query = client.query('SELECT * FROM "Vagas" WHERE id=($1)', [id]);
    // Stream results back one row at a time
    query.on('row', (row) => {
      result=row;	     	
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
			done();			
      return res.json(result);
    });
  });	
}	

// Add vaga 
module.exports.addVaga = (req, res) => {  
  const results = [];
  const idEvento = req.params.id;
  // Grab data from http request
  const data = {nome: req.body.nome, quantidade: req.body.quantidade, descricao: req.body.descricao,
                categoria: req.body.categoria, valor: req.body.valor};  
  // Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    // SQL Query > Insert Data
  client.query('INSERT INTO "Vagas" (nome,quantidade,descricao,'+
    'categoria,valor,"idEvento") values($1, $2, $3, $4, $5, $6)',
    [data.nome,data.quantidade,data.descricao, data.categoria, data.valor, 
     idEvento]);
    // SQL Query > Select Data
    const query = client.query('SELECT * FROM "Vagas" ORDER BY id ASC');
    // Stream results back one row at a time
    query.on('row', (row) => {
      results.push(row);
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
}

  // Update vaga 
  module.exports.updateVaga = (req,res) => {
    const results=[];	
    const id = req.params.id;
    // Grab data from http request
    const data = {nome: req.body.nome, quantidade: req.body.quantidade, descricao: req.body.descricao,
        categoria: req.body.categoria, valor: req.body.valor};
    // Get a Postgres client from the connection pool
    pg.connect(connectionString, (err, client, done) => {
      // Handle connection errors
      if(err) {
        done();
        console.log(err);
        return res.status(500).json({success: false, data: err});
      }
      // SQL Query > Update Data
      client.query('UPDATE "Vagas" SET nome=($1), quantidade=($2), descricao=($3), categoria=($4), valor=($5)  WHERE id=($6)',
      [data.nome,data.quantidade,data.descricao, data.categoria, data.valor, id]);
      // SQL Query > Select Data
      const query = client.query('SELECT * FROM "Vagas" where id=($1)',[id]);
      // Stream results back one row at a time
      query.on('row', (row) => {
        results.push(row);
      });
      // After all data is returned, close connection and return results
      query.on('end', function() {
      done();   
      return res.json(results);	  
      });
  });
}


  // Delete Vaga
module.exports.removeVaga = (req, res) => {
  const results = [];
  // Grab data from the URL parameters
  const id = req.params.id;
  // Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {
  // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    // SQL Query > Delete Data
    client.query('DELETE FROM "Vagas" WHERE id=($1)', [id]);
    // SQL Query > Select Data
    var query = client.query('SELECT * FROM "Vagas" ORDER BY id ASC');
    // Stream results back one row at a time
    query.on('row', (row) => {
      results.push(row);
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
}

