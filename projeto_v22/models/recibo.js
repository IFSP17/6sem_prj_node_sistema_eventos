const pg = require('pg');
var LocalStorage = require('node-localstorage').LocalStorage,
localStorage = new LocalStorage('./scratch');
const connectionString = process.env.DATABASE_URL || 'postgres://postgres:123456@localhost:5432/Evento';

// Adiciona recibo
module.exports.addRecibo = (req, res) => {  
  var results = [];
  var result, totalConvites, valorTotal, valorHomem, valorMulher, valorIdoso, valorCrianca, valorEstudante;
  const id = req.params.id;
  const idEvento = req.params.idEvento;
 
  // Grab data from http request   
  const data = {countHomem: req.body.countHomem, countMulher: req.body.countMulher, countCrianca: req.body.countCrianca, 
    countIdoso: req.body.countIdoso,countEstudante: req.body.countEstudante, cartaoCredito:req.body.cartaoCredito,
     cartaoNum:req.body.cartaoNum, cartaoCod:req.body.cartaoCod, cartaoDataEx: req.body.cartaoDataEx};  

  totalConvites= (data.countHomem+data.countMulher+data.countCrianca+data.countIdoso+data.countEstudante);

  // Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }

     const query = client.query('SELECT * FROM "Evento" WHERE id=($1)', [idEvento]);   
    // Stream results back one row at a time
    query.on('row', (row) => {
      result=row; 
      valorHomem = parseFloat(result.valorHomem);
      valorMulher =parseFloat(result.valorMulher);
      valorIdoso = parseFloat(result.valorIdoso);
      valorCrianca =parseFloat(result.valorCrianca); 
      valorEstudante =parseFloat(result.valorEstudante);      
     
      valorTotal= (parseInt(data.countHomem) * valorHomem) + (parseInt(data.countMulher)*valorMulher) + (parseInt(data.countCrianca)*valorCrianca) +(parseInt(data.countIdoso)*valorIdoso +(parseInt(data.countEstudante)*valorEstudante));

      client.query('UPDATE "Evento" SET "quantConvites"=  "quantConvites" - ($1)',
      [totalConvites]); 
   
      client.query('INSERT INTO "Recibo"("quantConviteIdoso", "quantConviteCrianca", "totalConvites", parcelas, "valorParcela",'+
      '"cartaoCredito", "cartaoNum", confirmacao, "idEvento","valorTotal","idUsuario", "quantConviteHomem", "quantConviteMulher",'+
      '"quantConviteEstudante", "cartaoCod", "cartaoDataEx") values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16)',
      [data.countIdoso, data.countCrianca, totalConvites,data.parcelas, data.valorParcela, data.cartaoCredito, data.cartaoNum, null, idEvento, valorTotal, id,
       data.countHomem, data.countMulher, data.countEstudante, data.cartaoCod, data.cartaoDataEx]);

       client.query('SELECT * FROM "Recibo" ORDER BY id ASC');
       results.push(row);
    });    

     query.on('end', () => {
     done();    
     console.log("dataaa"+JSON.stringify(data));     
     return res.json(results);  
    });     
  });
}

module.exports.searchEventosAdicionados = (req, res) => {
const results = [];
// Grab data from the URL parameters
const data = {tipoPesquisa:req.body.tipoPesquisa, valorPesquisa:req.body.valorPesquisa}  
// Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    } 

    if(data.tipoPesquisa=='Nome'){      
      // SQL Query > Select Data
      var query = client.query('SELECT * FROM "Evento" WHERE nome=($1)', [data.valorPesquisa]);   
      // Stream results back one row at a time
      query.on('row', (row) => {
        results.push(row);     
      });
      // After all data is returned, close connection and return results
      query.on('end', () => {
        done();      
        return res.json(results);
      });
    }
  });
}

//Seleciona eventos anexados
module.exports.getEventosAdicionados = (req, res) => {
const id = req.params.id;
const results = [];

// Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }              
    // SQL Query > Select Data
    const query = client.query('SELECT * FROM "Evento" INNER JOIN "Recibo" ON "Evento".id="Recibo"."idEvento" and "Recibo"."idUsuario"=($1)',[id]);   
    // Stream results back one row at a time
    query.on('row', (row) => {
      results.push(row);     
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
      done();      
      return res.json(results);
    }); 
  });
}

//Seleciona recibo referente a um evento
module.exports.getRecibos = (req, res) => {  
  const idUser = req.params.idUser;
  const idEvento =req.params.idEvento; 
  const results = [];  
  // Grab data from the URL parameters 
  // Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {
  // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }           
    // SQL Query > Select Data
    const query = client.query('SELECT * FROM "Recibo" where "idUsuario"=($1) and "idEvento"=($2)',[idUser,idEvento]);   
    // Stream results back one row at a time
    query.on('row', (row) => {
      results.push(row);   
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
      done();      
      return res.json(results);
    });   
  });
}
  
 // Atualiza recibo
 module.exports.updateRecibo = (req,res) => {
  const results=[];	 
  const id = req.params.id;  
  // Grab data from http request
  const data = {totalConvites:req.body.totalConvites, quantConviteHomem: req.body.quantConviteHomem,quantConviteMulher: req.body.quantConviteMulher,
     quantConviteCrianca: req.body.quantConviteCrianca, quantConviteIdoso: req.body.quantConviteIdoso, parcelas:req.body.parcelas, 
     valorParcela:req.body.valorParcela, cartaoCredito:req.body.cartaoCredito, cartaoNum: req.body.cartaoNum, valorTotal: req.body.valorTotal,
     quantConviteEstudante: req.body.quantConviteEstudante, cartaoCod: req.body.cartaoCod, cartaoDataEx:req.body.cartaoDataEx};
   // Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    // SQL Query > Update Data
    client.query('UPDATE "Recibo" SET "quantConviteIdoso"=($1), "quantConviteCrianca"=($2), "totalConvites"=($3), parcelas=($4), "valorParcela"=($5),'+
    '"cartaoCredito"=($6), "cartaoNum"=($7),"valorTotal"=($8), "quantConviteHomem"=($9), "quantConviteMulher"=($10), "quantConviteEstudante"=($11),'+
    '"cartaoCod"=($12), "cartaoDataEx"=($13) WHERE id=($14)',
    [data.quantConviteIdoso, data.quantConviteCrianca, data.totalConvites,data.parcelas, data.valorParcela, data.cartaoCredito, data.cartaoNum,data.valorTotal, 
    data.quantConviteHomem, data.quantConviteMulher, data.quantConviteEstudante, data.cartaoCod, data.cartaoDataEx, id]);
    // SQL Query > Select Data
    const query = client.query('SELECT * FROM "Recibo" where id=($1)',[id]);
    // Stream results back one row at a time
    query.on('row', (row) => {
      results.push(row);  
    });
    // After all data is returned, close connection and return results
    query.on('end', function() {
    done();   
    return res.json(results);    
  }); 
  }); 
}

// DELETE USUÁRIO
module.exports.removeRecibo = (req, res) => {
  const results = [];
  // Grab data from the URL parameters
  const id = req.params.id;
  // Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {
  // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    // SQL Query > Delete Data
    client.query('DELETE FROM "Recibo" WHERE id=($1)', [id]);
    // SQL Query > Select Data
    var query = client.query('SELECT * FROM "Recibo" ORDER BY id ASC');
    // Stream results back one row at a time
    query.on('row', (row) => {
      results.push(row);
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
}
