const pg = require('pg');
const connectionString = process.env.DATABASE_URL || 'postgres://postgres:123456@localhost:5432/Evento';

const client = new pg.Client(connectionString);
client.connect();

//Salva eventos numa lista
module.exports.addVaga = (req, res) => {    
    var results = [];
    const idUser = req.params.idUser;
    const idEvento = req.params.idEvento;   
    // Get a Postgres client from the connection pool
    pg.connect(connectionString, (err, client, done) => {
        // Handle connection errors
        if(err) {
          done();
          console.log(err);
          return res.status(500).json({success: false, data: err});
        }
        // SQL Query > Insert Data
      client.query('INSERT INTO "Vagas_Salvas"("idEvento","idUsuario") values($1, $2)',
      [idEvento, idUser]);
        // SQL Query > Select Data
        const query = client.query('SELECT * FROM "Vagas_Salvas" ORDER BY "idEvento" ASC');
        // Stream results back one row at a time
        query.on('row', (row) => {
          results.push(row);
        });
        // After all data is returned, close connection and return results
        query.on('end', () => {
          done();
          return res.json(results);
        });
    });
}    
        
//Seleciona eventos salvos
module.exports.getVagasSalvas = (req, res) => {   
    var results = [];
    const id = req.params.id;    
    // Get a Postgres client from the connection pool
    pg.connect(connectionString, (err, client, done) => {
      // Handle connection errors
        if(err) {
            done();
            console.log(err);
            return res.status(500).json({success: false, data: err});
        }      
 
       const query =  client.query('SELECT * FROM "Vagas" INNER JOIN "Vagas_Salvas" ON "Vagas_Salvas"."idUsuario"=($1)',[id]);
        query.on('row', (row) => {
            results.push(row);                                           
        });
        // After all data is returned, close connection and return results
        query.on('end', () => {
            done();                
            return res.json(results);           
        });
    }); 
}

 // DELETE evento salvo
 module.exports.removeVagasSalvas = (req, res) => {
   const results = [];   
    // Grab data from the URL parameters
    const id = req.params.id;       
    // Get a Postgres client from the connection pool
    pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
      if(err) {
        done();
        console.log(err);
        return res.status(500).json({success: false, data: err});
      }
      // SQL Query > Delete Data
      client.query('DELETE FROM "Vagas_Salvas" WHERE "id"=($1)', [id]);
      // SQL Query > Select Data
      var query = client.query('SELECT * FROM "Vagas_Salvas" ORDER BY "id" DESC');
      // Stream results back one row at a time
      query.on('row', (row) => {
        results.push(row);
      });
      // After all data is returned, close connection and return results
      query.on('end', () => {
        done();
        return res.json(results);
      });
    });    
  }
  
//Get evento salvo by id 
module.exports.getVagasSalvasById = (req, res) => {
  const results = [];
  // Grab data from the URL parameters
  const idUser = req.params.idUser;
  const idEvento = req.params.idEvento;   
  // Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {
  // Handle connection errors
  if(err) {
    done();
    console.log(err);
    return res.status(500).json({success: false, data: err});
  }
  // SQL Query > Delete Data
  var query = client.query('SELECT FROM "Vagas_Salvas" WHERE "idUsuario"=($1) and "idEvento"=($2)', [idUser, idEvento]);
  query.on('row', (row) => {
    results.push(row);
  });
  // After all data is returned, close connection and return results
  query.on('end', () => {
    done();
    return res.json(results);
  });
});
}