var fixaMenu;

function fixarMenu(){
	

	if (fixaMenu==undefined) 
	{
		return fixaMenu=1;			
	}	

	else if (fixaMenu==1)
	{
		return fixaMenu=undefined;
	}
}


function mostra() {

	document.getElementById('conteudo').style.display = 'block';
	document.getElementById('navbar').style.border = 'none';
	document.getElementById('navbar').style.background = 'rgba(0,0,0,0.0)';
	document.getElementById('navbar').style.boxShadow = '0px 0px 0px rgba(0,0,0,0.0)';
	document.getElementById('btnMenu').style.opacity = '0';				
}


function esconde() {

	if (fixaMenu == 1){		

		document.getElementById('conteudo').style.display = 'block';
		document.getElementById('navbar').style.border = 'none';
		document.getElementById('navbar').style.background = 'rgba(0,0,0,0.0)';
		document.getElementById('navbar').style.boxShadow = '0px 0px 0px rgba(0,0,0,0.0)';
		document.getElementById('btnMenu').style.opacity = '0';									
	}
	else
	{
		document.getElementById('conteudo').style.display = 'none';
		document.getElementById('navbar').style.background = 'rgba(0,0,0,0.3)';
		document.getElementById('navbar').style.borderRight = '#E8E8E8';
		document.getElementById('navbar').style.borderRightStyle = 'double';		
		document.getElementById('btnMenu').style.opacity = '1';
		document.getElementById('navbar').style.boxShadow = '5px 5px 5px rgba(0,0,0,0.3)';				
	}
}


$(document).ready(function(){ 
    $('#characterLeft').text('2000  caracteres');
    $('#message').keydown(function () {
        var max = 2000;
        var len = $(this).val().length;
        if (len >= max) {
            $('#characterLeft').text('You have reached the limit');
            $('#characterLeft').addClass('red');
            $('#btnContato').addClass('disabled');            
        } 
        else {
            var ch = max - len;
            $('#characterLeft').text(ch + ' characters left');
            $('#btnContato').removeClass('disabled');
            $('#characterLeft').removeClass('red');            
        }
    });    
});
