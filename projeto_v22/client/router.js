var myApp = angular.module('myApp',['ngRoute','ngMask']);

myApp.config(function($routeProvider){
			
	$routeProvider.when('/',{				
		controller:'UsuariosController',
		templateUrl: 'views/login.html'	
	})				

	.when('/add/usuario',{		
		controller:'UsuariosController',	
		templateUrl: 'views/addUsuario.html'		
	})

	.when('/listar/usuarios', {
		controller:'UsuariosController',
		templateUrl: 'views/listarUsuarios.html'
	})

	.when('/detalhes/usuario',{	
		controller:'UsuariosController',
		templateUrl: 'views/detalhesUsuario.html'		
	})	
			
	.when('/editar/usuario/:id',{
		controller:'UsuariosController',
		templateUrl: 'views/editarUsuario.html'
	})

	.when('/logout',{
		controller:'UsuariosController',
		templateUrl: 'views/login.html'
	})	

	//EVENTO
	.when('/add/evento',{		
		controller:'EventosController',
		templateUrl: 'views/addEvento.html'		
	})

	.when('/listar/eventos',{		
		controller:'EventosController',
		templateUrl: 'views/listarEventos.html'		
	})	

	.when('/detalhes/evento/:id',{		
		controller:'EventosController',
		templateUrl: 'views/detalhesEvento.html'		
	})	

	//Evento cadastrado pelo usuário
	.when('/listar/eventos/cadastrados', {
		controller:'EventosController',
		templateUrl: 'views/listarEventosCadastrados.html'
	})

	.when('/detalhes/evento/cadastrado/:id',{		
		controller:'EventosController',
		templateUrl: 'views/detalhesEventoCadastrado.html'		
	})
	
	.when('/editar/evento/:id',{
		controller:'EventosController',
		templateUrl: 'views/editarEvento.html'
	})

	//Evento que o usuário adicionou ao home page			
	.when('/detalhes/evento/adicionado/:id',{		
		controller:'EventosController',
		templateUrl: 'views/detalhesEventoAdicionado.html'		
	})			
	
	.when('/listar/eventos/adicionados',{
		controller:'ReciboController',
		templateUrl: 'views/listarEventosAdicionados.html'
	})	
	
	//Eventos_Salvos
	.when('/listar/eventos/salvos',{
		controller:'EventosSalvosController',
		templateUrl: 'views/listarEventosSalvos.html'		
	})

	//Vagas
	.when('/add/vaga/:id',{		
		controller:'VagasController',	
		templateUrl: 'views/addVaga.html'		
	})

	.when('/editar/vaga/:id',{
		controller:'VagasController',
		templateUrl: 'views/editarVaga.html'
	})

	.when('/listar/vagas/salvas',{
		controller:'VagasSalvasController',
		templateUrl: 'views/listarVagasSalvas.html'
	})

	.when('/listar/vaga/:id',{
		controller:'VagasController',
		templateUrl: 'views/detalhesEvento.html'
	})

	//Contato
	.when('/contato',{		
		templateUrl: 'views/contato.html'
	})

	//Sobre
	.when('/sobre',{		
		templateUrl: 'views/sobre.html'
	})

	//Ajuda
	.when('/ajuda',{		
		templateUrl: 'views/ajuda.html'
	})
	
	$routeProvider.otherwise({
		redirectTo: '/'		
	});
	
});	

//Usuario
myApp.directive("formAddUsuario", function() {
	return {
		restrict : "C", //Procura classe com o nome da diretiva especificada
		templateUrl : "views/formUsuario.html"
	};
});

myApp.directive("formEditarUsuario", function() {
	return {
		restrict : "C", 
		templateUrl : "views/formUsuario.html"
	};
});

//Evento
myApp.directive("formAddEvento", function() {
	return {
		restrict : "C", 
		templateUrl : "views/formEvento.html"
	};
});

myApp.directive("formEditarEvento", function() {
	return {
		restrict : "C", 
		templateUrl : "views/formEvento.html"
	};
});

myApp.directive("formListarEventos", function() {
	return {
		restrict : "C", 
		templateUrl : "views/formListarEventos.html"
	};	
});	

myApp.directive("formDetalhesEvento", function() {
	return {
		restrict : "C", 
		templateUrl : "views/formDetalhesEvento.html"
	};
});	

myApp.directive("formListarEventosBuscados", function() {
	return {
		restrict : "C", 
		templateUrl : "views/formListarEventosBuscados.html"
	};
});

//Eventos Cadastrados pelo usuário
myApp.directive("formListarEventosCadastrados", function() {
	return {
		restrict : "C", 
		templateUrl : "views/formListarEventos.html"
	};
});

//Recibo
myApp.directive("formAddRecibo", function() {
	return {
		restrict : "C", 
		templateUrl : "views/formAddRecibo.html"
	};
});

myApp.directive("formListarRecibos", function() {
	return {
		restrict : "C", 
		templateUrl : "views/formListarRecibos.html"
	};	
});
	
/*Vagas*/
myApp.directive("formAddVaga", function() {
	return {
		restrict : "C", 
		templateUrl : "views/formVaga.html"
	};
});

myApp.directive("formListarVagas", function() {
	return {
		restrict : "C", 
		templateUrl : "views/formListarVagas.html"
	};
});

myApp.directive("formEditarVaga", function() {
	return {
		restrict : "C", 
		templateUrl : "views/formVaga.html"
	};
});

myApp.directive("formListarVagasSalvas", function() {
	return {
		restrict : "C", 
		templateUrl : "views/formListarVagas.html"
	};
});

