var myApp = angular.module('myApp');

myApp.controller('ContatosController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams){
    console.log('ContatosController loaded...');	
    
    $scope.enviarEmailContato = function(){
		
		var emailRemetente = $scope.contato.email;
		alert("Aguarde um momento!");		
		
		$http.post('/api/contato/'+emailRemetente,$scope.contato)
		.then((response)=>{
			alert ("Email enviado com sucesso!");
			window.location.reload();
		})
		.catch((response)=>{
			console.log("Error:"+JSON.stringify(response));
			alert ("Ocorreu um erro ao enviar seu e-mail!");
		});		
    }
}]);