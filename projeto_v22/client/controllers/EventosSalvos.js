var myApp = angular.module('myApp');

myApp.controller('EventosSalvosController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams){
    console.log('EventosSalvosController loaded...');   

    $scope.addEventoSalvo = function(){
        var idEvento= $routeParams.id;	
        var idUser = sessionStorage.getItem("id");       
        $http.post('/add/evento/salvo/'+idEvento+'/'+idUser)
            .then((response)=>{				
               window.location.href='#/listar/eventos/salvos';          
            })
            .catch((response)=>{
                console.log("Error:"+JSON.stringify(response));
                alert ("Erro ao salvar evento, tente novamente!");
            });
    }	

    $scope.listarEventosSalvos = function(){	
        var id = sessionStorage.getItem("id");        
        $http.get('/listar/eventos/salvos/'+id)
            .then((response)=>{	               			
                $scope.eventos = response.data;
            })
            .catch((response)=>{
                console.log("Error:"+JSON.stringify(response));
                alert ("Erro ao listar eventos salvos, tente novamente!");
            });
    }
    
    $scope.deletarEventoSalvo = function(idEvento){	
        var id = sessionStorage.getItem("id");       	

        confirmacao = confirm("Você deseja mesmo excluir esse evento da lista de eventos salvos?");	
		if(confirmacao){
            $http.delete('/deletar/evento/salvo/'+id+'/'+idEvento)
                .then((response)=>{	

                    alert ("Evento excluído com sucesso!");			
                    window.location.reload(); 
                    
                })
                .catch((response)=>{
                    console.log("Error:"+JSON.stringify(response));
                    alert ("Erro excluir evento, tente novamente!");
                });
        }
    }      

}]);