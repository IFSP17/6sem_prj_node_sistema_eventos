var myApp = angular.module('myApp');

myApp.controller('EmailsController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams){
    console.log('EmailsController loaded...');	
    
    $scope.enviarEmail = function(){
		
		var emailRemetente = sessionStorage.getItem("email"); 
		alert("Aguarde um momento!");		
		
		$http.post('/api/email/'+emailRemetente,$scope.email)
		.then((response)=>{
			alert ("Email enviado com sucesso!");
			window.location.reload();
		})
		.catch((response)=>{
			console.log("Error:"+JSON.stringify(response));
			alert ("Ocorreu um erro ao enviar seu e-mail!");
		});		
    }
}]);