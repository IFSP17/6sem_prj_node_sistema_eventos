var myApp = angular.module('myApp');

myApp.controller('VagasSalvasController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams){
    console.log('VagasSalvasController loaded...');   

    $scope.addVagaSalva = function(){
        var idEvento= $routeParams.id;	
        var idUser = sessionStorage.getItem("id");       
        $http.post('/add/vaga/salva/'+idEvento+'/'+idUser)
            .then((response)=>{				
               window.location.href='#/listar/vagas/salvas';          
            })
            .catch((response)=>{
                console.log("Error:"+JSON.stringify(response));
                alert ("Erro ao salvar vaga, tente novamente!");
            });
    }	

    $scope.listarVagasSalvas = function(){	
        var id = sessionStorage.getItem("id");        
        $http.get('/listar/vagas/salvas/'+id)
            .then((response)=>{                           			
                $scope.vagas = response.data;
            })
            .catch((response)=>{
                console.log("Error:"+JSON.stringify(response));
                alert ("Erro ao listar vagas salvas, tente novamente!");
            });
    }
    
    $scope.deletarVagaSalva = function(id){              	

        confirmacao = confirm("Você deseja mesmo excluir essa vaga da lista?");	
		if(confirmacao){
            $http.delete('/deletar/vaga/salva/'+id)
                .then((response)=>{
                    alert ("Vaga excluída com sucesso!");			
                    window.location.reload(); 
                    
                })
                .catch((response)=>{
                    console.log("Error:"+JSON.stringify(response));
                    alert ("Erro ao excluir vaga da lista de vagas salvas, tente novamente!");
                });
        }
    }    
    
    // $scope.detalhesEvento = function(id){

    //     $http.get('/detalhes/evento/'+id)
    //     .then((response)=>{	
    //         $scope.evento = response.data;				
    //     })
    //     .catch((response)=>{
    //         console.log("Error:"+JSON.stringify(response));
    //     });
    // }

}]);