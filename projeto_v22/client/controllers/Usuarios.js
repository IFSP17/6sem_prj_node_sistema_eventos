var myApp = angular.module('myApp');

myApp.controller('UsuariosController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams){
	console.log('UsuariosController loaded...');	

	//Seleciona usuário
	$scope.verUsuario = function (){		
		if (sessionStorage.getItem("id")==null || sessionStorage.getItem("id")==""){			
			window.location.href='/'
			alert("Erro ao exibir dados do usuário, tente novamente!");
		}
	}	

	//Seleciona usuário
	$scope.listarUsuarios = function(){		
		$http.get('/listar/usuarios')
		.then((response)=>{				
			$scope.usuario = response.data;
		})
		.catch((response)=>{
			console.log("Error:"+JSON.stringify(response));
			alert("Erro ao listar usuário, tente novamente!");
		});
	}

	//Seleciona usuário pelo id
	$scope.detalhesUsuario = function(){	
		var id = sessionStorage.getItem("id");	
		$http.get('/detalhes/usuario/'+id)
		.then((response)=>{				
			$scope.usuario = response.data;
		})
		.catch((response)=>{
			console.log("Error:"+JSON.stringify(response));
			alert("Erro ao exibir dados do usuário, tente novamente!");
		});
	}

	//Adiciona usuário	
	$scope.addUsuario = function(){

		if($scope.usuario.senha!=$scope.usuario.rsenha)
		{
			alert("As senhas devem ser iguais!");
			
			$scope.usuario.senha="";
			$scope.usuario.rsenha="";
		}

		else{
		
			$http.post('/add/usuario/', $scope.usuario)
				.then((response)=>{				
					window.location.href='#/';
				})
				.catch((response)=>{
					console.log("Error:"+JSON.stringify(response));
					alert("Erro ao cadastrar usuário, tente novamente!");
				});
		}	
	}

	//Autentica usuário
	$scope.authenticateUsuario = function(){		
		$http.post('/api/usuarios/'+$scope.email, $scope.usuario).success(function(response){			
			var status=JSON.stringify(response.status);
			if (status=='true'){
				window.location.href='#/eventos';					
			}
			else{
				alert("Email ou senha incorretos");
			}				
		});
	}	

	//Atualiza usuário
	$scope.editarUsuario = function(){
		var id = sessionStorage.getItem("id");		
		$http.put('/editar/usuario/'+id, $scope.usuario)
		.then((response)=>{				
			window.location.href='#/detalhes/usuario';
		})
		.catch((response)=>{
			console.log("Error:"+JSON.stringify(response));
			alert("Erro ao editar usuário, tente novamente!");
		});
	}

	//Remove usuário
	$scope.deletarUsuario = function(id){
		var id = sessionStorage.getItem("id");
		confirmacao = confirm("Você deseja mesmo excluir seu cadastro permanentemente?");	
		if(confirmacao){
			$http.delete('/deletar/usuario/'+id)

			.then((response)=>{				
				window.location.href='#/';
				alert ("Usuário excluído com sucesso!");
			})
			.catch((response)=>{
				console.log("Error:"+JSON.stringify(response));
				alert("Erro ao tentar excluir usuário, tente novamente!")
			});
		}
		else {window.location.href='#/detalhes/usuario'};
	}

	//Loga usuário
	$scope.loginUsuario =(user)=>{	
		console.log("User:"+user);
		$http.post('/usuario/login',user)		
		.then((response)=>{			
			var status=JSON.stringify(response.status);
				console.log("Sucess: "+JSON.stringify(response));
				sessionStorage.setItem("id",response.data.id);
				sessionStorage.setItem("email",response.data.email);
				console.log("Session: "+sessionStorage.getItem("email")+" id: "+sessionStorage.getItem("id"));
							
				window.location.href='#/listar/eventos/adicionados';
						
		})
		.catch((error)=>{

			console.log('Error: ' + JSON.stringify(error));
			$scope.user={};
			$scope.message="Usuário ou senha inválidos!";
		})
	}	

	//Logout usuário
	$scope.logoutUsuario = ()=>{		
		
		$http.get('/usuario/logout')
		//Caso retorne 200 OK
		.then((response)=>{
			console.log("Sucess: "+JSON.stringify(response));
			sessionStorage.clear();
			$scope.autLogin=false;
			window.location.href='/';
		})
		//Caso retorne 500 ERROR
		.catch((error)=>{
			console.log('Error: ' + JSON.stringify(error));
			$scope.autLogin=true;
			window.location.href='/';
		});
	}
	
}]);

