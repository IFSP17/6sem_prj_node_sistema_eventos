var myApp = angular.module('myApp');

myApp.controller('EventosController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams){
	console.log('EventosController loaded...');

		$scope.listarEventos = function(){		
			$http.get('/listar/eventos')
			.then((response)=>{				
				$scope.eventos = response.data;				
			})
			.catch((response)=>{
				console.log("Error:"+JSON.stringify(response));
				alert("Erro ao tentar listar eventos, tente novamente!")
			});
		}

		$scope.listarEventosCadastrados = function(){
			var id = sessionStorage.getItem("id");
			
			$http.get('/listar/eventos/cadastrados/'+id)
			.then((response)=>{					
				$scope.eventos = response.data;			
			})
			.catch((response)=>{
				console.log("Error:"+JSON.stringify(response));
				alert("Erro ao tentar listar eventos cadastrados, tente novamente!")
			});
		}		

		$scope.detalhesEvento = function(){
			var id = $routeParams.id;
			var idUser = sessionStorage.getItem("id");
			
			$http.get('/detalhes/evento/'+id)
			.then((response)=>{	
				$scope.evento = response.data;
				
				if(response.data.gratuito==true){
					document.getElementById('btnComprarIngresso').disabled=true;
					document.getElementById('btnComprarIngresso').title="Esse evento é gratuito";
				}
			})
			.catch((response)=>{
				console.log("Error:"+JSON.stringify(response));
				alert("Erro ao tentar exibir detalhes do evento, tente novamente!")
			});

			$http.get('/detalhes/evento/salvo/'+idUser+'/'+id)
			.then((response)=>{
				console.log("Sucess: "+JSON.stringify(response));				
				if(response.data!='')
				{
					document.getElementById('ativaBtn').disabled=true;
					document.getElementById('ativaBtn').title="Você já salvou esse evento em sua lista";					
				}
				
			})
			//Caso retorne 500 ERROR
			.catch((error)=>{
				console.log('Error: ' + JSON.stringify(error));
			});
		}

			$scope.detalhesEventoCadastrado = function(){
				var id = $routeParams.id;
				var idUser = sessionStorage.getItem("id");
				
				$http.get('/detalhes/evento/cadastrado/'+id)
				.then((response)=>{	
					$scope.evento = response.data;		
					
				})
				.catch((response)=>{
					console.log("Error:"+JSON.stringify(response));
					alert("Erro ao tentar exibir detalhes do evento, tente novamente!")
				});	
			}

		$scope.detalhesEventoAdicionado = function(){
			var id = $routeParams.id;
			var idUser = sessionStorage.getItem("id");
			
			$http.get('/detalhes/evento/'+id)
			.then((response)=>{	
				$scope.evento = response.data;
				 
				var data= response.data.data.split("/");
				var date = new Date();						
		
				if(parseInt(data[2])<date.getFullYear()){
					alert("O recibo não pode ser mais atualizado!"+data[2]+"/"+date.getFullYear())				
					$scope.btnAtualizaRecibo=false;			
				}		
		
				else if(parseInt(data[1])<date.getMonth()+1){					
					alert("O recibo não pode ser mais atualizado!" +data[1]+"/"+date.getMonth()+1)
					$scope.btnAtualizaRecibo=false;				
				}
		
				else if(parseInt(data[0])<date.getDate()){					
					alert("O recibo não pode ser mais atualizado!"+data[0]+"/"+date.getDate())
					$scope.btnAtualizaRecibo=false;				
				}
				else 
				$scope.btnAtualizaRecibo=true;
	
			})
			.catch((response)=>{
				console.log("Error:"+JSON.stringify(response));
				alert("Erro ao tentar exibir detalhes do evento, tente novamente!")
			});

			$http.get('/detalhes/evento/salvo/'+idUser+'/'+id)
			.then((response)=>{
				console.log("Sucess: "+JSON.stringify(response));				
				if(response.data=='')
				{
					$scope.ativaBtn = true;					
				}
				else
					$scope.ativaBtn = false;
			})
			//Caso retorne 500 ERROR
			.catch((error)=>{
				console.log('Error: ' + JSON.stringify(error));
			});		
			
		}

		$scope.addEvento = function(){
			var id = sessionStorage.getItem("id");			
			var date = new Date();					
			var ver1,ver2,ver3, ver4;				
			
			var data = $scope.evento.data.split("/");
			var dataLimitePag, dataVendas;		
			
			if ($scope.evento.dataLimitePag==undefined){
				dataLimitePag=0;
				ver2=true;								
			}

			if ($scope.evento.dataVendas==undefined){
				dataVendas=0;				
				ver3=true;
				ver4=true;								
			}

			ver1=verificaData(data, date);	
			
			if(ver1==false){
				document.getElementById('data').focus();
				document.getElementById('data').style.borderColor = "red";
			}
			
			if(dataLimitePag!=0){
				dataLimitePag = $scope.evento.dataLimitePag.split("/");
				ver2= verificaData(dataLimitePag, date);

				if(ver2==false){
					document.getElementById('dataLimitePag').focus();
					document.getElementById('dataLimitePag').style.borderColor = "red";
				}				
			}

			if(dataVendas!=0){				
				dataVendas = $scope.evento.dataVendas.split("/");				
				ver3= verificaData(dataVendas, date);
				ver4= verificaData2(data,dataVendas);

				if(ver3==false || ver4==false){
					document.getElementById('dataVenda').focus();
					document.getElementById('dataVenda').style.borderColor = "red";
				}
			}

			if (ver1==true && ver2==true && ver3==true && ver4==true){

				$http.post('/add/evento/'+id, $scope.evento)
				.then((response)=>{
					console.log("Sucess: "+JSON.stringify(response));	
					window.location.href='#/listar/eventos/cadastrados';			
					
				})
				//Caso retorne 500 ERROR
				.catch((error)=>{
					console.log('Error: ' + JSON.stringify(error));
					alert("Erro ao tentar adicionar evento, tente novamente!")
				});								
				
			}	
			
		}

		$scope.editarEvento = function(){
			var id= $routeParams.id;

			$http.put('/editar/evento/'+id, $scope.evento)
			.then((response)=>{				
				window.location.href='#/listar/eventos/cadastrados';
			})
			.catch((response)=>{
				console.log("Error:"+JSON.stringify(response));
				alert("Erro ao tentar editar evento, tente novamente!")
				
			});
		}	

		$scope.deletarEvento = function(id){			
			confirmacao = confirm("Você deseja mesmo excluir esse evento permanentemente?");	
			if(confirmacao){
			
				$http.delete('/deletar/evento/'+id)
				.then((response)=>{				
					window.location.href='#/listar/eventos/cadastrados';
					alert ("Evento excluído com sucesso!");
				})
				.catch((response)=>{
					console.log("Error:"+JSON.stringify(response));
					alert("Erro ao tentar deletar evento, talvez esse evento tenha dependências e não possa ser excluído!")
				});
			}		
		}		

		$scope.pesquisarEventos = function(){	
			
			$http.post('/pesquisar/eventos/',$scope.event)
			.then((response)=>{				
				$scope.evento = response.data;
			})
			.catch((response)=>{
				console.log("Error:"+JSON.stringify(response));
				alert("Erro ao tentar pesquisar evento, tente novamente!")
			});
		}
		
		// $scope.pesquisarEventoAdicionado = function(){			

		// 	$http.post('/pesquisar/eventos/adicionados/',$scope.evento)
		// 	.then((response)=>{				
		// 		$scope.eventos = response;
		// 	})
		// 	.catch((response)=>{
		// 		console.log("Error:"+JSON.stringify(response));
		// 		alert("Erro ao tentar pesquisar evento adicionado, tente novamente!")
		// 	});
		// }

		/*Verifica se a data está correta*/
		function verificaData(data, date){							
			var mes= date.getMonth()+1;					
			if(parseInt(data[2])<date.getFullYear()){							
				alert("Insira uma data válida maior que a atual!");
				return false;				
			}
				else if(parseInt(data[2])==date.getFullYear()){					
					if(parseInt(data[1])<mes){						
						alert("Insira uma data válida maior que a atual!");				
						return false;
					}
					else if (parseInt(data[1])==mes){
						
						if(parseInt(data[0])<date.getDate()){							
							alert("Insira uma data válida maior que a atual!");							
							return false;
					}
				}
			}
			return true;
		}	
		
		function verificaData2(data, dataVendas){

			if(data[2]<dataVendas[2]){
				alert("A data de venda dos ingressos não pode ser maior que a data do evento!");						
				return false;
			}

			if(data[2]==dataVendas[2] && data[1]<dataVendas[1]){
				alert("A data de venda dos ingressos não pode ser maior que a data do evento!");			
				return false;
			}

			if(data[2]==dataVendas[2] && data[1]==dataVendas[1] &&  data[0]<dataVendas[0]){
				alert("A data de venda dos ingressos não pode ser maior que a data do evento!");				
				return false;
			}

			return true;
		}	

}]);

