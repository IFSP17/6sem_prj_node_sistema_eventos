var myApp = angular.module('myApp');

myApp.controller('ReciboController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams){
	console.log('ReciboController loaded...');
	
	//Seleciona eventos anexados por um usuário	
	$scope.listarEventosAdicionados = function(){
		var id = sessionStorage.getItem("id");		
		$http.get('/listar/eventos/adicionados/'+id)
		.then((response)=>{				
			$scope.eventos = response.data;
		})
		.catch((response)=>{
			console.log("Error:"+JSON.stringify(response));
			alert("Erro ao listar os eventos que você pretende participar, tente novamente!");
		});
	}

	//Adiciona recibo
	$scope.addRecibo = function(){
		var id = sessionStorage.getItem("id");
		
		var total=($scope.recibo.countHomem*$scope.evento.valorHomem)+($scope.recibo.countMulher*$scope.evento.valorMulher)+($scope.recibo.countCrianca*$scope.evento.valorCrianca)
		+($scope.recibo.countIdoso*$scope.evento.valorIdoso) + ($scope.recibo.countEstudante*$scope.evento.valorEstudante);
		
		if(total<=$scope.evento.parcela1 && $scope.recibo.parcelas>$scope.evento.quantParcela1 || total<=$scope.evento.parcela2 && $scope.recibo.parcelas>$scope.evento.quantParcela2 || total<=$scope.evento.parcela3 && $scope.recibo.parcelas>$scope.evento.quantParcela3){
			alert("O seu valor total não pode ser dividido por essa quantidade de parcelas!");
		}
		
		else if(total==0 && $scope.recibo.parcelas>0){
			alert("Isso não é permitido!");
		}
		
		else{
			$http.post('/add/recibo/'+id+'/'+$scope.evento.id, $scope.recibo)
			.then((response)=>{	
						
				window.location.href='#/listar/eventos/adicionados';
			})
			.catch((response)=>{
				console.log("Error:"+JSON.stringify(response));
				alert("Erro ao efetuar compra, tente novamente!");
				
			});
		}
	}

	//Busca evento anexado por nome ou categoria
	// $scope.pesquisarEventoAdicionado = function(){
	// 	var id = sessionStorage.getItem("id");	
	// 	$http.post('/pesquisar/eventos/adicionado',$scope.evento)
	// 	.then((response)=>{				
	// 		$scope.eventos = response;
	// 	})
	// 	.catch((response)=>{
	// 		console.log("Error:"+JSON.stringify(response));
	// 		alert("Erro ao pesquisar evento que você pretende participar, tente novamente!");
			
	// 	});
	// }
	

	//Seleciona recibos referentes a um determinado evento 
	$scope.listarRecibos = function(){		
		var idUser = sessionStorage.getItem("id");
		var idEvento= $routeParams.id;		

			$http.get('/listar/recibos/'+idUser +'/'+idEvento)
			.then((response)=>{		
				$scope.recibos = response.data;			
			})
			.catch((response)=>{
				console.log("Error:"+JSON.stringify(response));
				alert("Erro ao listar seus recibos de compra, tente novamente!");				
			});	
			
			// var data= dataEvento.split("/");
			// var date = new Date();			
		
	
			// if(parseInt(data[2])==date.getFullYear || parseInt(data[2])>date.getFullYear){
			// 	alert("Esse recibo não pode ser mais atualizado!")
			// 	document.getElementById('btnAtualizaRecibo').dusabled= "true";			
			// }		
	
			// else if(parseInt(data[1])==date.getMonth+1 || parseInt(data[1])>date.getMonth+1){
			// 	alert("Esse recibo não pode ser mais atualizado!")
			// 	document.getElementById('btnAtualizaRecibo').dusabled= "true";			
			// }
	
			// else if(parseInt(data[0])==date.getDate || parseInt(data[0])>date.getDate){
			// 	alert("Esse recibo não pode ser mais atualizado!")
			// 	document.getElementById('btnAtualizaRecibo').dusabled= "true";			
			// }
			
	}


	// $scope.deletarRecibo = function(){	
	// 	var id= recibo.id;	

	// 	confirmacao = confirm("Você deseja mesmo excluir esse recibo?");	
	// 	if(confirmacao){			
	// 	$http.delete('/deletar/recibo/'+id)
	// 	.then((response)=>{	
	// 		window.location.href='#/listar/eventos/adicionados';		
	// 	})		
	// 	.catch((response)=>{
	// 		console.log("Error:"+JSON.stringify(response));
	// 		alert("Erro ao deletar recibo, tente novamente!");			
	// 	});
	// }
	// }

	$scope.editarRecibo = function(recibo){	
		var id= recibo.id;	
		
		$http.put('/editar/recibo/'+id, recibo)
		.then((response)=>{	
			window.location.href='#/listar/eventos/adicionados';		
		})
		.catch((response)=>{
			console.log("Error:"+JSON.stringify(response));
			alert("Erro ao tentar atualizar recibo, tente novamente!");			
		});	
	}
}]);

