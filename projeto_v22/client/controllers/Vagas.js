var myApp = angular.module('myApp');

myApp.controller('VagasController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams){
	console.log('VagasController loaded...');
	
	//Adiciona vaga	
	$scope.addVaga = function(){
		var id =  $routeParams.id;		
		$http.post('/add/vaga/'+id, $scope.vaga)
		.then((response)=>{				
			window.location.href='#/listar/eventos/cadastrados';
		})
		.catch((response)=>{
			console.log("Error:"+JSON.stringify(response));
			alert ("Erro ao adicionar vaga, tente novamente!");
		});
	}

	
	//Seleciona vagas
	$scope.listarVagas = function(){	
		var idEvento =  $routeParams.id;
		var idUser = sessionStorage.getItem("id");			
		$http.get('/listar/vagas/'+idEvento)
		.then((response)=>{					
			$scope.vagas = response.data;			
			if(response.data=='')
			{
				document.getElementById('btnParticipar').disabled=true;
				document.getElementById('btnParticipar').title="Esse evento não possui vagas de emprego";								
			}
		})
		.catch((response)=>{
			console.log("Error:"+JSON.stringify(response));
			alert ("Erro ao listar vagas, tente novamente!");
		});

		$http.get('/listar/vagas/salvas/'+idUser+'/'+idEvento)
		.then((response)=>{
			console.log("Sucess: "+JSON.stringify(response));				
			if(response.data!='')
			{
				document.getElementById('ativaBtnVaga').disabled=true;
				document.getElementById('ativaBtnVaga').title="Você já salvou essa vaga em sua lista";					
			}
		})
		//Caso retorne 500 ERROR
		.catch((error)=>{
			console.log('Error: ' + JSON.stringify(error));							
		});
	}

	$scope.listarVagasAdicionadas = function(){	
		var idEvento =  $routeParams.id;
		var idUser = sessionStorage.getItem("id");			
		$http.get('/listar/vagas/'+idEvento)
		.then((response)=>{					
			$scope.vagas = response.data;			
		})
		.catch((response)=>{
			console.log("Error:"+JSON.stringify(response));
			alert ("Erro ao listar vagas, tente novamente!");
		});
	}

	//Seleciona vaga pelo id
	$scope.detalhesVaga = function(){	
		var id =  $routeParams.id;	
		$http.get('/detalhes/vaga/'+id)
		.then((response)=>{				
			$scope.vaga = response.data;
		})
		.catch((response)=>{
			console.log("Error:"+JSON.stringify(response));
			alert ("Erro ao exibir dados da vaga, tente novamente!");
		});
	}	

	//Atualiza vaga
	$scope.editarVaga = function(){
		var id =  $routeParams.id;		
		$http.put('/editar/vaga/'+id, $scope.vaga)
		.then((response)=>{				
			window.location.href='#/listar/eventos/cadastrados';
		})
		.catch((response)=>{
			console.log("Error:"+JSON.stringify(response));
			alert ("Erro ao editar vaga, tente novamente!");
		});
	}

	//Remove vaga
	$scope.deletarVaga = function(id){

		confirmacao = confirm("Você deseja mesmo excluir essa vaga permanentemente?");	
		if(confirmacao){
			$http.delete('/deletar/vaga/'+id)
			.then((response)=>{				
				window.location.href='#/listar/eventos/cadastrados';
				alert ("Vaga excluída com sucesso!");
			})
			.catch((response)=>{
				console.log("Error:"+JSON.stringify(response));
				alert ("Erro ao deletar vaga, tente novamente!");
			});
		}		
	}	

	$scope.verVaga = function(){
			
		var idEvento =  $routeParams.id;			
		$http.get('/listar/vagas/'+idEvento)
		.then((response)=>{					
			if (JSON.stringify(response.data)=="[]" || JSON.stringify(response.data)==null){
			
			 var confirma=confirm("Você não possui nenhuma vaga de serviço cadastrada nesse evento, você deseja cadastrar uma vaga agora?")
			  if(confirma==true){
				window.location.href='#/add/vaga/'+idEvento;
			  }
			}	
		})
		.catch((response)=>{
			console.log("Error:"+JSON.stringify(response));
		});
	}
}]);

