const express = require('express');
const app = express();
const bodyParser = require('body-parser');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
const pg = require('pg');
const path = require('path');

var passport = require('passport');
var flash = require('connect-flash');
var expressSession = require('express-session');

app.use(express.static(__dirname+'/client'));
app.use(bodyParser.json());

var router = express.Router();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'client')));

app.use(flash());

app.use(expressSession({
  secret : process.env.SESSION_SECRET || 'codate 2014',
  saveUninitialized : false,
  cookie: { maxAge : 1800000 }
}));
app.use(passport.initialize());
app.use(passport.session());

// Initialize Passport
var initPassport = require('./passport/init');
initPassport(passport);

var routesClientes = require('./routes')(passport);
//Define rotas a serem disponibilizadas na URL
app.use('/', routesClientes);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;

